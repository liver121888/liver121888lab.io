---
name: Postoffice Sim
tools: [ROS, YOLO, Pedsim, MoveIt, SLAM]
image: ../img/projects/crowds.png
description: Finish delivery task in Gazebo, with social force driven crowd.
---
# _Postoffice Sim_

{% include elements/video.html id="qCTjceOs6T4" %}

{% include elements/button.html link="https://github.com/liver121888/postoffice_sim" text="Repository" %}

In this side project, I try to simulate crowds and robot in the same environment, and let the robot pick a package from the postoffice and deliver to the houses.

The crowds is simulated by [SPACiSS](https://github.com/maprdhm/SPACiSS), a crowd simulation based on [pedsim_ros](https://github.com/srl-freiburg/pedsim_ros), the ROS version of Christian Gloor's libpedsim, the implementation of social force model of [Helbing et. al](https://arxiv.org/abs/cond-mat/9805244).

The robot model I used is from the chapter 5 of [ROS Robotics Projects - Second edition](https://github.com/PacktPublishing/ROS-Robotics-Projects-SecondEdition), published by Packt.

The robot has to identify the suitcase, so I used [darknet_ros](https://github.com/leggedrobotics/darknet_ros), the ROS implementation of YOLO (V3).

I currently stuck at this [problem](https://answers.ros.org/question/411862/moveit-perception-pipeline-octomap-position-is-wrong-with-gazebo-and-kinect/), if you have any thoughts or you want to cooperate with me on the project, pls let me know.
