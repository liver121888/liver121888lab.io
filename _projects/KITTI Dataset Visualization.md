---
name: KITTI Dataset Visualization
tools: [Visualization, ROS]
image: ../img/projects/KITTI.png
description: Visualization of the KITTI dataset in ROS.
---
# _KITTI Dataset Visualization_

{% include elements/video.html id="n_GqWNppx08" %}

{% include elements/button.html link="https://github.com/liver121888/Self-driving-Project" text="Code" %}

Followed this [tutorial](https://www.youtube.com/playlist?list=PLDV2CyUo4q-L4YlXUWDytZPz9a8cAWXST) to publish all the topics.