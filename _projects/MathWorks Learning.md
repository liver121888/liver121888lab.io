---
name: MathWorks Learning
tools: [MATLAB, Simulink]
image: ../img/projects/MathWorks Learning/TechTalks.png
description: Learn MathWorks product online, such as MATLAB, Simulink.
---
# _MathWorks Learning_

Mainly follows Brian Douglas' fantastic [MATLAB Tech Talks tutorials](https://engineeringmedia.com/videos), with some others from MathWorks webinars and [Self-Paced Online Courses](https://matlabacademy.mathworks.com).

{% include elements/button.html link="https://github.com/liver121888/MathWorks-learning" text="Repository" %}

- [_MathWorks Learning_](#mathworks-learning)
  - [Autonomous Navigation](#autonomous-navigation)
  - [Kalman Filter Virtual Lab](#kalman-filter-virtual-lab)
  - [Robust Control](#robust-control)
  - [System Identification](#system-identification)
  - [Understanding Sensor Fusion and Tracking](#understanding-sensor-fusion-and-tracking)


## Autonomous Navigation

In this section, I followed the [playlist](https://youtube.com/playlist?list=PLn8PRpmsu08rLRGrnF-S6TyGrmcA2X7kg) to learn the basics of Autonomous Navigation.

When talking about localization part, we get to practice the method in Gazebo simulation, with a familiar robot, Turtlebot3 Burger.

![AMCL](../img/projects/MathWorks Learning/Autonomous Navigation/gazebo.png)

We let the robot wander in the room for AMCL to work.

![AMCL](../img/projects/MathWorks Learning/Autonomous Navigation/AMCL.gif)

<p style="text-align:center; font-style: italic">AMCL with initial pose estimate</p>

![AMCL](../img/projects/MathWorks Learning/Autonomous Navigation/AMCL_no_initial.gif)

<p style="text-align:center; font-style: italic">AMCL without initial pose estimate</p>

We could see that without the initial pose estimate, the robot could be at upper part of the room or lower part of the room, and not until the first turn did the AMCL localize the robot. On the other hand, with initial pose estimate, AMCL could quickly localize robot at the upper part of the room.

## Kalman Filter Virtual Lab

In this section, I followed the [playlist](https://youtube.com/playlist?list=PLn8PRpmsu08pzi6EMiYnR-076Mh-q3tWr) to implement Kalman Filter and EKF in the MATLAB to estimate the motion of a pendulum.

{% include elements/button.html link="https://htmlpreview.github.io/?https://github.com/liver121888/MathWorks-learning/blob/master/MATLAB%20Tech%20Talks/Kalman%20Filter%20Virtual%20Lab/Kalman_Filter_Virtual_Lab.html" text="Code+Notes" %}

Kalman Filter is widely used in sensor fusion and estimation topic, and the virtual lab system is shown as below:

![Block Diagram](../img/projects/MathWorks Learning/Kalman Filter/Block.png)

If the pendulum swings at small angle and the input torque is zero, the system can be approximately linear. Also, there are noise exist in both processing and measurement.

![Measurement](../img/projects/MathWorks Learning/Kalman Filter/Measured.gif)

We could implement a Kalman filter using the model and the measurement input, to estimate the pendulum motion in a less noisy way.

![Kalman Filter](../img/projects/MathWorks Learning/Kalman Filter/Kal.gif)

However, as the initial angle of the pendulum increases to 55°, the linear Kalman Filter would start to lose accuracy.

![Nonlinear](../img/projects/MathWorks Learning/Kalman Filter/NP.png)

To address the nonlinearity in the system, we could use Extended-Kalman Filter to linearize the system in place.

![EKF Wave](../img/projects/MathWorks Learning/Kalman Filter/EKF Wave.png)

Demux 1 is the actual response, Demux 2 is the raw measured data (tampered by noise), Demux 3 is the estimate of the linear Kalman Filter, and Demux 4 is the result from the Extended-Kalman Filter.

Let's take a closer look.

![EKF Wave](../img/projects/MathWorks Learning/Kalman Filter/EKF Detail.png)

The green estimate from the EKF is closer to the actual response (Yellow) than the measurement (Blue) does. Well done!

In the tutorial, we also tested the effect of filter's initial estimate and the filter's noise estimate. After the tutorial, I have a better understanding toward Kalman Filter, including the pros and cons of Kalman Filter, EKF, and Unscented Kalman filter.

## Robust Control

## System Identification

## Understanding Sensor Fusion and Tracking

