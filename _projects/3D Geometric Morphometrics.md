---
name: 3D Geometric Morphometrics on sabertoothed felids
tools: [Linear Algebra, Generalized Procrustes Analysis, PCA, 3D Slicer, SlicerMorph, Latex]
image: ../img/projects/Morph.gif
description: Conducting a Linear Algebra term project on a domain I am not familiar with.
---
# _3D Geometric Morphometrics on sabertoothed felids_

{% include elements/button.html link="https://link.springer.com/article/10.1007/s10914-021-09541-0" text="Reference Paper" %}

Recreated the experiment of a 3D Geometric Morphometrics paper using different tools, yield similar results.

{% include adobepdfapi.html url="https://raw.githubusercontent.com/liver121888/github-drive/master/Linear_Algebra_and_Its_Applications_in_Three_dimensional_Geometric_Morphometrics.pdf" id="adobe_dc_view_LinearAlgebra.pdf" name="Linear Algebra and Its Applications in Three-dimensional Geometric Morphometrics.pdf" mode="LIGHT_BOX" %}

Video of morphing along PC1, PC2:

![Morphing](../img/projects/Morph.gif)
