---
name: Ball Delivery Cart
tools: [Arduino, Raspberry Pi]
image: ../img/projects/Ball Delivery Cart.png
description: Raspberry pi and Arduino based mobile robot.
---
# _Ball Delivery Cart_

{% include elements/video.html id="lFlVofkHzSU" %}

{% include elements/button.html link="https://github.com/liver121888/NTUBME-2021-MechatronicsIV-FinalProject" text="Code" %}

In this project, my teammates and I built a mobile robot from scratch, to achieve the course goal—remote ball delivery.
We use Processing to design UI, incorporate command and video streaming in one window. The motors are controlled via Arduino and H-bridge using PWM.
The caster wheel sometimes got stuck in the crevices of the tiles, so we designed a more powerful forward and turning movement to get out of the situation.

{% include adobepdfapi.html url="https://raw.githubusercontent.com/liver121888/NTUBME-2021-MechatronicsIV-FinalProject/master/MechatronicsIV.pdf" id="adobe_dc_view_MechatronicsIV.pdf" name="MechatronicsIV.pdf" mode="LIGHT_BOX" %}
