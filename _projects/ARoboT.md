---
name: ARoboT
tools: [ROS, MoveIt, Motion Planning, Manipulator]
image: ../img/projects/ARoboT/arobotsetup.png
description: Autonomous Pixel Art Builder.
---
# _Autonomous Pixel Art Builder_

{% include elements/video.html id="M1CR32Me4oU" %}

{% include elements/button.html link="https://github.com/liver121888/ARoboT" text="Repository" %}

In this project, I led a team of 4 to build a pixel art builder robot.

The setup is as follow, the Panda arm takes building blocks from picking area and stack them inside building area to form a pixel art. The robot can generate a plan for a predefined target pixel art. 
After it execute each action, it will check the current building state using the webcam. If the state is not as expected, the robot will plan the recovery behavior using symbolic planning.

![setup](../img/projects/ARoboT/arobotsetup.png)

The figure below shows the building process.

![forward](../img/projects/ARoboT/forward.gif)

In the midway, we swapped the blocks.

![r1](../img/projects/ARoboT/r1.png)

The robot can sense the difference and replan the plan to get to the goal

![1](../img/projects/ARoboT/1.gif)

Here is another example. The robot expect the block on top left corner should be green but it's actually black, and there is a black block out of nowhere but satisfy to the plan. 

![r2](../img/projects/ARoboT/r2.png)

The robot can directly move blocks that's already in the art to save the number of actions. 

![2](../img/projects/ARoboT/2.gif)
