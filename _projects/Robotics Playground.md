---
name: Robotics Playground
tools: [ROS, Motion Planning, SLAM]
image: ../img/projects/Robotics Playground/playground.png
description: A repository full of motion planning and robot simulations.
---
# _Robotics Playground_

{% include elements/button.html link="https://github.com/liver121888/Robotics-Playground" text="Repository" %}

This forked repository follows the [tutorials](https://youtube.com/playlist?list=PL0sla3wvhSnYNAyp0-OQmTMyO2POZRSe-) created by Sakshay. The git submodules are not properly set at first, so I linked all the submodule for other people to use, and modified the READMEs for ROS beginners.

The tutorials covered several dominant method in motion planning including A*, DWA, RRT and some other knowledge in robotics. 

The spot_ws explaining the RRT algorithm cannot be launch at first, I figure my way thorough and hope it can help people using the spot simulations.

![Spot](../img/projects/Robotics Playground/spot_ws.gif)
