---
name: Stable Diffusion
tools: [ML, AIGC]
image: ../img/projects/Stable Diffusion/Stable Diffusion.gif
description: Dive into stable diffusion.
---
# _Stable Diffusion_

{% include elements/video.html id="-lz30by8-sU" %}

{% include elements/button.html link="https://colab.research.google.com/drive/1NYiHllO98gp-gnM7JMenyGQ6S94cz8z2?usp=sharing" text="Code" %}

Learning stable diffusion following the tutorial from Computerphile.

The original code is buggy, it took me a while to make the program working.

Stable diffusion can outperform GAN because it is easy to train, and with language models, we can achieve even more. Diffusion model opens an era for AIGC (AI Generated Content), and brought about the exponential growth of Novel AI, Midjourney, Meitu Ai, etc. 

The diffusion process can be similar to removing the noise from an noised image.

![Process](../img/projects/Stable Diffusion/Stable Diffusion.gif)

This video shows the denoising process:

_**"A sad Japanese lady in kimono weeping over a dying Black cat in a sunlit room, Ukiyo-e"**_

![Denoising](../img/projects/Stable Diffusion/Denoising.gif)

With stable diffusion's mix guidance feature, we can have impossible scene.

_**Combining frog and rabbit in same ratio**_

![Mix guidance](../img/projects/Stable Diffusion/Mix Guidance.png)

For more information, just watch computerphile's video.