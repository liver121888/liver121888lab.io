---
name: Steganography
tools: [Cryptology, Qt]
image: ../img/projects/DIP.png
# external_url: https://github.com/liver121888/NTUBME-2021-DIP-Assignments/tree/master/b07611001_FP
description: A Qt application using AES128 implementing DCT, DWT, LSB steganography.
---
# _Steganography_

{% include elements/button.html link="https://github.com/liver121888/NTUBME-2021-DIP-Assignments/tree/master/b07611001_FP" text="Code" %}

Image steganography has been used in many area. In this project, we propose a well-organized steganography application to do various method of steganography. The encrypting part of the application would include LSB steganography, DCT steganography, and DWT steganography; the hidden message would be encrypted with an AES key in 16 char as well as an IV (initial vector) for block cypher. The decrypting part of the application would include not only the decryption part with key and IV, but also some of the famous strategies used to detect steganography, such as histogram examination, image subtraction.

{% include adobepdfapi.html url="https://raw.githubusercontent.com/liver121888/NTUBME-2021-DIP-Assignments/master/b07611001_FP/FP_Li-Wei_Yang_b07611001.pdf" id="adobe_dc_view_FP_Li-Wei_Yang_b07611001.pdf" name="FP_Li-Wei_Yang_b07611001.pdf" mode="IN_LINE" %}
