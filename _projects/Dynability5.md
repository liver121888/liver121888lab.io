---
name: Dynability5
tools: [EMG, Manipulator, ML]
image: ../img/projects/Dyna.png
# external_url: https://github.com/liver121888/NTUME-2021-MMC-FinalProject
description: Using an SVM classifier that recognizes filter-conditioned EMG pattern to control a 5 DoF manipulator.
---
# _Dynability5_

{% include elements/video.html id="r5DFUHN26QI" %}

{% include elements/button.html link="https://github.com/liver121888/NTUME-2021-MMC-FinalProject" text="Code" %}

In this project, my teammates and I integrated an EMG signal processing circuit (including IA, HF, LP, NP, Voltage compression, Voltage shift) and a 5Dof manipulator (Dynamixel AX-12A) to deliver water bottles for disabled people. We trained the SVM classifier by modifying TA's code, and eventually reached 90% of accuracy. Our classifier suffered from the ambient 60HZ noise, and thus the performance is not well. Future improvements would try to solve the noise issue.

{% include elements/button.html link="https://github.com/visionbike/Digital-Bio-Signal-Analysis" text="TA's original code" %}