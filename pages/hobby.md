---
layout: page
title: Hobby
permalink: /hobby/
weight: 60
---

# Some of my dance videos

## CMU KPDC

{% include elements/video.html id="r8IXjtZZQMU" %}

## CMU Street Styles

{% include elements/video.html id="3uk92wOurnQ" %}

{% include elements/video.html id="dRm3BNIjTJk" %}

## CMU Dancers' Symposium

{% include elements/video.html id="MMNji_X3szM" %}

## Raven Huff

{% include elements/video.html id="zCz7YHUila8" %}

{% include elements/video.html id="dZPpfpoNbN0" %}

{% include elements/video.html id="FVgFmoOmrF8" %}

## NTUPDC 31

{% include elements/video.html id="bC0I-Rvj2k4" %}

{% include elements/video.html id="CCAnp4c3DLU" %}

{% include elements/video.html id="J268fqBcRP0" %}

{% include elements/video.html id="W-q3HXjTkAQ" %}

## HSNUDC 25

{% include elements/video.html id="Xqan1AKbaX8" %}

{% include elements/video.html id="y2X9iE31L4Y" %}

{% include elements/video.html id="U95BhSNaSes" %}

{% include elements/video.html id="rf35_lZMY4Y" %}
