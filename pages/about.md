---
layout: page
title: About
permalink: /about/
weight: 10
---

# **About**

## Autobiography
Liwei's research interest lies in robotics and reinforcement learning. To build robotics systems, Liwi has been trained in various subjects including Machine Learning, Automatic Control, and Computer Vision, etc. Funded by the Ministry of Science and Technology, he works on an undergraduate project to create a patient-adapting oral model for robot swabbing in high-fidelity simulation. His enthusiasm earned him the Presidential Award while taking several upper-division courses and master-level courses. His interest in robotics stemmed from the movie, _**Prometheus**_. The [scene](https://youtu.be/NUhjlb96mA8) where Dr. Shaw use the medical pod to do a cesarean section impressed him with the robot's dexterity and intelligence. To stay abreast of state-of-the-art technology, Liwei has participated in Taoyuan ROS summer school 2021 and Nvidia GTC 2022. He is currently exploring reinforcement learning in Isaac Gym and ROS.

## Education
<div class="row">
{% include about/education_timeline.html %}
</div>

## Experience

<div class="row">
{% include about/experience_timeline.html %}
</div>

## Skills

<div class="row">
{% include about/skills.html title="Programming Skills" source=site.data.programming-skills %}
{% include about/skills.html title="Other Skills" source=site.data.other-skills %}
</div>