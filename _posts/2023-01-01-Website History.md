---
title: Website History
tags: [Jekyll, Thoughts]
comments: true
style: border # style: fill/border (choose one only)
color: danger # color: primary/secondary/success/danger/warning/info/light/dark (blue/gray/green/red/yellow/cyan/white/black)
description: The story behind this website.
# external_url:
---

I started to develop this website around 2020 summer. At first, it was just a simple page with a lot of youtube videos embedded, as my playlist and the assignment for a git summer course in our lab.
{% include elements/button.html link="https://liver121888.github.io/liver121888_playlist/" text="Playlist" block="true" %}
![playlist](../img/posts/Website%20History/playlist.png)

But around 2021 winter, in my senior year, I migrated the website to the Jekyll theme as my portfolio. 
{% include elements/button.html link="https://liver121888-github-io.vercel.app/" text="Portfolio ver. 1" block="true" %}
![[portv1]](../img/posts/Website%20History/portv1.png)
Because the template theme used plugins not supported on the vanilla github page, I had to deploy it on Vercel, which is a cool experience.

The template has a lot of problems after I adapted to it, such as the mobile layout would be a total disaster. So in 2022 summer, after I graduated and have more time to prepare for school applications, I searched for great templates on both mobile and desktop. It leads me to choose the template I am currently with.
{% include elements/button.html link="https://liver121888.github.io/" text="Portfolio ver. 2" block="true" %}

But there are still problems: I would like my reader could view the pdf files on-site, so I need to embed them. The traditional embed method with object HTML tag cannot render pdf on mobile version. Also, I want my presentations could be embedded, too. I found a [Jekyll plugin](https://github.com/MihajloNesic/jekyll-pdf-embed) that could embed presentations, but again the plugin did not work on github page. After several days of trials in vain, I finally decided to migrate the website to gitlab for comprehensive Jekyll support, and here we go! You are looking at the deployment right now!

The Jekyll plugin I just mentioned, sadly, still cannot render pdf and will automatically download (quite annoying) the pdf on the mobile version, if the pdf is in github repositories (although it did embed presentations well on both mobile and desktop).
![[mobilepdf]](../img/posts/Website%20History/mobilepdf.jpg)

As a perfectionist, I finally found [Adobe PDF Embed API](https://developer.adobe.com/document-services/apis/pdf-embed/) as a solution. The Javascript API supports three types of embedding and it can fetch rawgithubusercontent well!

From working on this blog, I learned Jekyll, Liquid syntax, Javascript, Google Analytics and other web techs. The website still needs a lot of improvement, including friendly commenting mechanisms other than Disqus that are easy for anonymous comments. I will keep this website updated, stay tuned!
