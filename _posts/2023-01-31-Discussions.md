---
title: Discussions
tags: [Share, Peace, Love]
comments: true
style: border # style: fill/border (choose one only)
color: success # color: primary/secondary/success/danger/warning/info/light/dark (blue/gray/green/red/yellow/cyan/white/black)
description: A place for discussions.
# external_url:
---

Feel free to discuss anything in the comment box (powered by [CommentBox.io](https://commentbox.io/)).

Please be polite, with no racism and NSFW content.

After signing up for your account, you can leave your msg. <br/>
(CommentBox.io is not interested in your data, the data is for me to know who you are.)

Choose to leave anonymous msgs by checking the checkbox.

You can upvote or downvote a msg, if certain content offends you, pls flag the comment for me: <br/>
I will moderate the comment through the system.

