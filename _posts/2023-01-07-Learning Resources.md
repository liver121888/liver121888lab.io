---
title: Learning Resources
tags: [Learning, Share]
comments: true
style: fill # style: fill/border (choose one only)
color: info # color: primary/secondary/success/danger/warning/info/light/dark (blue/gray/green/red/yellow/cyan/white/black)
description: Some learning resources I used.
# external_url:
---

- [Control](#control)
- [ML](#ml)
- [NLP](#nlp)
- [Latex](#latex)
- [Robotics](#robotics)


# Control

[MATLAB Tech Talks](https://engineeringmedia.com/videos)

[Well written notebook explaining PID (CMU)](https://youtu.be/dR0nTvZqabQ)

# ML

[Hung-Yi Lee ML](https://speech.ee.ntu.edu.tw/~hylee/ml/2021-spring.php)

# NLP

[Vivian ADL](https://youtube.com/playlist?list=PLOAQYZPRn2V5yumEV1Wa4JvRiDluf83vn)

# Latex

[Dr. Trefor Bazett](https://youtube.com/playlist?list=PLHXZ9OQGMqxcWWkx2DMnQmj5os2X5ZR73)

# Robotics

[Pei-Chun Lin Robotics](https://www.coursera.org/learn/robotics1) + [Dynamics Part](https://youtube.com/playlist?list=PLI2Is7Tote9wpcFMKSj75jNSoL-Z66BLD)

